# Kinematic Simulation

Project to create a kinematic model of the hang glider frame using Open Modelica simulation language.

# Getting started
First install Open Modelica including OMEdit from https://www.openmodelica.org/. Load the files hang_glider.mo and Frame_Model_Pack.mo in OMEdit. Open hang_glider.mo and select the "Simulate with animation" option. The model will take some time to simulate depending on the speed of your computer, but you should eventually see the animated view of the hang glider frame as follows:

![HG_modelica.gif](./.resources/HG_modelica.gif)

# Description of model
The model "hang_glider" contains a frame model, adds a pilot to it and supports it in space. A force can be applied between the pilot and the control frame corner to simulate a control input. The model is supported by a pair of spring dampers, one on each leading edge tube. The location of these was adjusted so that the frame hangs roughly in a flying attitude. The nose is constrained in x and y but is completely free in z and in all axes of rotation.

![image.png](./.resources/image.png)

The frame model is subdivided into blocks representing the key structural elements (keel, leading edges, control frame and sail tension). All these blocks are contained in the package "Frame_Model_Pack".

![image_1.png](./.resources/image_1.png)

**Key assumptions:**

-Wires are modelled as spring/dampers. The stiffness is calculated from the elastic modulus of steel and the wire diameter. However treating them as circular steel rods would overestimate their stiffness since the strands have twist and do not cover 100% of the CSA. A factor of 0.9 is used to account for this. Since the standard spring damper model is used this means that in principle these can apply compressive loads. As long as the model is correctly set up so that all wires are in tension anyway this doesn't cause a problem, but does need checking when the model is set up that all wires are indeed in tension.

-Tubes are not properly modelled as flexible beams. Making a flexible beam element in Modelica should be pretty straightforward but would be a fair bit of work. So in this model they are treated as rigid elements which 'flex' in one direction only by rotating around a joint at the end. The stiffness of this joint is calculated from the tube id/od, E and the length at which the most significant load is applied. This gives the same exact result as the proper beam bending equations for deflection at the point at which the load is applied. The angle at this point or the deflection at other points isn't exactly right, although it is close. The LEI components can flex in the vertical direction (not the horizontal direction, although they're freely pin-jointed to the nose plates anyway) inboard of the cross tube junction. The can flex in the horizontal direction outboard of the cross tube junction. The LEO's can flex in the horizontal direction only. The keel flexes in the vertical direction only with the force application point assumed as the top of uprights.

-Closed kinematic loops of rigidly constrained rigid tubes seems to cause problems (despite what the Modelica documentation claims!). For this reason the control frame is split at one corner and the noseplate>leading edge>cross tube>leveller>cross tube>leading edge> noseplate loop is broken at the cross tube/leveller junctions. At these points a very strong zero length spring is used to clamp them together. During the simulation, these springs stay at pretty close to zero length (fractions of a mm) so these effectively act like ball joints. For the control frame I think this approximation is fine. For the cross leveller, it does mean that the cross tubes take no bending load, which is fine for a kingposted glider, but not exactly the case on a topless.

-Sail tension is taken as acting along a line from the outer tip of the LEO to the TE of the keel pocket. The level of tension is calculated as a function of distance by assuming the sail follows an arc between these two points. The sail therefore puts a force on the keel (and tip) at a tangent to this arc. The vertical component is taken as a fraction of the glider weight and thus the horizontal component is calculated. This calculation is done in the sail tension model.

# Example use case: Active versus passive billow shift
The first use of this model was to answer the question of whether a weight shift movement pulls the keel across, causing a difference in sail tension and so a difference in billow (active billow shift) or whether the weight shift just starts the movement, causes a difference in sail tension, which then pulls the keel across (passive billow shift). The model can be tweaked to look at which has more effect.
The sail tension model works out the horizontal component of the sail tension as a function of the distance to the tip and also the vertical component of force. The vertical component comes from the weight force from the supports. In the first version of the model the force from the two supports is averaged and this average is send to both sides sail tensions, so the sail on both sides has the same vertical force component in this model. In the Frame_Model_Pack_Passive version of the model, only the force from the left support is fed to the left sail and only the force from the right support is fed to the right sail. So in this version of the model when the pilot weight shifts to the left the left sail has a higher vertical force component than the right, so a higher horizontal force component, so pulls the keel across.The effect of this can be seen below:

![image_2.png](./.resources/image_2.png)

Differences in Left/Right vertical component of sail tension in passive model

![image_3.png](./.resources/image_3.png)

Differences in left/right horizontal component of sail tension in active and passive models (not huge, but significant).

![image_5.png](./.resources/image_5.png)

Differences in angle of sail to horizontal at the keel in active and passive models (quite significant).

![image_4.png](./.resources/image_4.png)

Difference in amount of keel movement for active only and passive case. For active only model the movement is about 5mm, when passive effect is included then it's 20mm.

**Conclusion**
Keel movement is several times greater when passive effect from weightshift is included, so passive effects look much more significant that the direct effect of 'pulling the keel across'. This is only for the initial control input. As the glider rolls, aerodynamic effects take over, but this will likely increase the passive billow shift still further.

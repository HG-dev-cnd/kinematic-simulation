model Flex_Tube
  import Modelica.Constants.*;
  parameter Modelica.SIunits.Length dia_i = 0.05 "Internal diameter of tube";
  parameter Modelica.SIunits.Length dia_o = 0.052 "External diameter of tube";
  parameter Modelica.SIunits.ModulusOfElasticity E = 72e9 "Young's modulus of tube material";
  parameter Modelica.SIunits.SecondMomentOfArea I = (pi/4)*((dia_o/2)^4-(dia_i/2)^4) "2nd moment of area of tube";
  parameter Modelica.SIunits.Length length = 1 "Length of tube";
  parameter Modelica.SIunits.Length length_force = 1 "Point on tube that force is applied to";
  parameter Modelica.SIunits.RotationalSpringConstant stiffness_vert = 3*E*I/length_force "Vertical stiffness of tube";
  
  Modelica.Mechanics.MultiBody.Parts.BodyCylinder bodyCylinder(density = 2400, diameter = dia_o, innerDiameter = dia_i, r = {length, 0, 0})  annotation(
    Placement(visible = true, transformation(origin = {4, 0}, extent = {{-42, -42}, {42, 42}}, rotation = 0)));
  Modelica.Mechanics.MultiBody.Joints.Revolute revolute(n = {0, 1, 0}, phi(fixed = true, start = 0), useAxisFlange = true, w(fixed = true, start = 0))  annotation(
    Placement(visible = true, transformation(origin = {-130, 0}, extent = {{-18, -18}, {18, 18}}, rotation = 0)));
  Modelica.Mechanics.MultiBody.Interfaces.Frame_a frame_a annotation(
    Placement(visible = true, transformation(origin = {-242, 0}, extent = {{-16, -16}, {16, 16}}, rotation = 0), iconTransformation(origin = {-242, 0}, extent = {{-16, -16}, {16, 16}}, rotation = 0)));
  Modelica.Mechanics.Rotational.Components.Spring vert_stiff(c = stiffness_vert, phi_rel0 = 0)  annotation(
    Placement(visible = true, transformation(origin = {-138, 58}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Mechanics.Rotational.Components.Damper vert_damp(d = stiffness_vert / 10) annotation(
    Placement(visible = true, transformation(origin = {-138, 86}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Mechanics.MultiBody.Parts.FixedTranslation fixedTranslation(r = {length_force, 0, 0})  annotation(
    Placement(visible = true, transformation(origin = {-2, 68}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Mechanics.MultiBody.Interfaces.Frame_b frame_b annotation(
    Placement(visible = true, transformation(origin = {182, 22}, extent = {{-16, -16}, {16, 16}}, rotation = 0), iconTransformation(origin = {182, 22}, extent = {{-16, -16}, {16, 16}}, rotation = 0)));
  Modelica.Mechanics.MultiBody.Interfaces.Frame_b frame_b1 annotation(
    Placement(visible = true, transformation(origin = {90, 68}, extent = {{-16, -16}, {16, 16}}, rotation = 90), iconTransformation(origin = {-2, 58}, extent = {{-16, -16}, {16, 16}}, rotation = 90)));
equation
  connect(revolute.frame_b, bodyCylinder.frame_a) annotation(
    Line(points = {{-112, 0}, {-36, 0}, {-36, 0}, {-38, 0}}, color = {95, 95, 95}));
  connect(frame_a, revolute.frame_a) annotation(
    Line(points = {{-242, 0}, {-148, 0}, {-148, 0}, {-148, 0}}));
  connect(revolute.support, vert_stiff.flange_a) annotation(
    Line(points = {{-140, 18}, {-164, 18}, {-164, 58}, {-148, 58}, {-148, 58}}));
  connect(vert_stiff.flange_b, revolute.axis) annotation(
    Line(points = {{-128, 58}, {-116, 58}, {-116, 18}, {-130, 18}, {-130, 18}}));
  connect(vert_damp.flange_a, vert_stiff.flange_a) annotation(
    Line(points = {{-148, 86}, {-164, 86}, {-164, 58}, {-148, 58}, {-148, 58}}));
  connect(vert_damp.flange_b, vert_stiff.flange_b) annotation(
    Line(points = {{-128, 86}, {-116, 86}, {-116, 58}, {-128, 58}, {-128, 58}}));
  connect(bodyCylinder.frame_a, fixedTranslation.frame_a) annotation(
    Line(points = {{-38, 0}, {-60, 0}, {-60, 68}, {-12, 68}, {-12, 68}}, color = {95, 95, 95}));
  connect(fixedTranslation.frame_b, frame_b1) annotation(
    Line(points = {{8, 68}, {90, 68}}, color = {95, 95, 95}));
  connect(bodyCylinder.frame_b, frame_b) annotation(
    Line(points = {{46, 0}, {126, 0}, {126, 22}, {182, 22}}, color = {95, 95, 95}));
  annotation(
    Icon(coordinateSystem(extent = {{-500, -100}, {500, 100}})),
    Diagram(coordinateSystem(extent = {{-500, -100}, {500, 100}})),
    uses(Modelica(version = "3.2.3")));
end Flex_Tube;
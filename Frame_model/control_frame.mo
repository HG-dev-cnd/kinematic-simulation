model control_frame
  Modelica.Mechanics.MultiBody.Parts.BodyCylinder upright(density = 2400, diameter = 0.0284, innerDiameter = 0.025, r = {1.689, 0, 0}) annotation(
    Placement(visible = true, transformation(origin = {-90, 2}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  Modelica.Mechanics.MultiBody.Parts.BodyCylinder bodyCylinder(density = 2400, diameter = 0.0284, innerDiameter = 0.025, r = {1.689, 0, 0}) annotation(
    Placement(visible = true, transformation(origin = {64, 4}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  Modelica.Mechanics.MultiBody.Parts.BodyCylinder bodyCylinder1(density = 2400, diameter = 0.0284, innerDiameter = 0.025, r = {1.345, 0, 0}) annotation(
    Placement(visible = true, transformation(origin = {-18, -28}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Mechanics.MultiBody.Joints.Revolute revolute(n = {0, 1, 0}, phi(start = 1.18759), stateSelect = StateSelect.always) annotation(
    Placement(visible = true, transformation(origin = {38, 46}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Mechanics.MultiBody.Joints.Revolute revolute1(n = {0, 1, 0}, phi(start = 4.32918)) annotation(
    Placement(visible = true, transformation(origin = {-74, 46}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Mechanics.MultiBody.Joints.Revolute BOU_R(n = {0, 1, 0}, phi(start = 1.18759)) annotation(
    Placement(visible = true, transformation(origin = {32, -28}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Mechanics.MultiBody.Joints.RevolutePlanarLoopConstraint BOU_L(n = {0, 1, 0}) annotation(
    Placement(visible = true, transformation(origin = {-76, -28}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Mechanics.MultiBody.Parts.FixedTranslation TOU_R(r = {0.041, 0, 0}) annotation(
    Placement(visible = true, transformation(origin = {2, 40}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Mechanics.MultiBody.Parts.FixedTranslation TOU_L(r = {0.041, 0, 0}) annotation(
    Placement(visible = true, transformation(origin = {-34, 32}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  inner Modelica.Mechanics.MultiBody.World world(n = {0, 0, -1}) annotation(
    Placement(visible = true, transformation(origin = {-66, 82}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  connect(revolute.frame_b, bodyCylinder.frame_a) annotation(
    Line(points = {{48, 46}, {64, 46}, {64, 14}}, color = {95, 95, 95}));
  connect(revolute1.frame_a, upright.frame_a) annotation(
    Line(points = {{-84, 46}, {-84, -1}, {-90, -1}, {-90, 12}}, color = {95, 95, 95}));
  connect(BOU_R.frame_b, bodyCylinder.frame_b) annotation(
    Line(points = {{42, -28}, {64, -28}, {64, -6}}));
  connect(BOU_R.frame_a, bodyCylinder1.frame_b) annotation(
    Line(points = {{22, -28}, {-8, -28}}));
  connect(bodyCylinder1.frame_a, BOU_L.frame_b) annotation(
    Line(points = {{-28, -28}, {-66, -28}}, color = {95, 95, 95}));
  connect(BOU_L.frame_a, upright.frame_b) annotation(
    Line(points = {{-86, -28}, {-86, -25}, {-90, -25}, {-90, -8}}, color = {95, 95, 95}));
  connect(TOU_R.frame_b, revolute.frame_a) annotation(
    Line(points = {{12, 40}, {21, 40}, {21, 46}, {28, 46}}));
  connect(revolute1.frame_b, TOU_L.frame_a) annotation(
    Line(points = {{-64, 46}, {-57, 46}, {-57, 32}, {-44, 32}}, color = {95, 95, 95}));
  connect(world.frame_b, TOU_R.frame_a) annotation(
    Line(points = {{-56, 82}, {-8, 82}, {-8, 40}, {-8, 40}}));
  connect(world.frame_b, TOU_L.frame_b) annotation(
    Line(points = {{-56, 82}, {-24, 82}, {-24, 32}, {-24, 32}}, color = {95, 95, 95}));
  annotation(
    Icon(coordinateSystem(extent = {{-500, -100}, {500, 100}})),
    Diagram(coordinateSystem(extent = {{-500, -100}, {500, 100}})),
    uses(Modelica(version = "3.2.3")),
    experiment(StartTime = 0, StopTime = 2, Tolerance = 1e-06, Interval = 0.004));
end control_frame;
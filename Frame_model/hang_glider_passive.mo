model hang_glider_passive
  import Modelica.SIunits.Conversions.*;
  import Modelica.Constants.*;
  parameter Modelica.SIunits.Length NPx = 0.05795 "Set back of leading edge hinge along keel";
  parameter Modelica.SIunits.Length NPy = 0.0635 "Distance outboard of leading edge hinge from keel";
  parameter Modelica.SIunits.Angle noseAngle = from_deg(127.4) "Nose angle of glider";
  parameter Modelica.SIunits.Length susp_point = 2.5 "Distance to supsension point along leading edge";
  inner Modelica.Mechanics.MultiBody.World world(n = {0, 0, -1}) annotation(
    Placement(visible = true, transformation(origin = {-284, 28}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Mechanics.MultiBody.Parts.FixedTranslation longi(r = {2, 0, 0}, width = 0.025) annotation(
    Placement(visible = true, transformation(origin = {-174, 28}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Mechanics.MultiBody.Parts.FixedRotation np_support_L(angle = -to_deg(noseAngle / 2), animation = false, n = {0, 0, 1}, r = {NPx, -NPy, 0}, width = 0.025) annotation(
    Placement(visible = true, transformation(origin = {-182, -12}, extent = {{-10, -10}, {10, 10}}, rotation = 270)));
  Modelica.Mechanics.MultiBody.Parts.FixedTranslation support_L(color = {0, 200, 0}, r = {susp_point, 0, 0}, width = 0.025) annotation(
    Placement(visible = true, transformation(origin = {-158, -58}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Mechanics.MultiBody.Parts.FixedRotation np_support_R(angle = to_deg(noseAngle / 2), animation = false, n = {0, 0, 1}, r = {NPx, NPy, 0}, width = 0.025) annotation(
    Placement(visible = true, transformation(origin = {-186, 54}, extent = {{-10, -10}, {10, 10}}, rotation = 90)));
  Modelica.Mechanics.MultiBody.Parts.FixedTranslation support_R(color = {200, 0, 0}, r = {susp_point, 0, 0}, width = 0.025) annotation(
    Placement(visible = true, transformation(origin = {-164, 88}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Mechanics.MultiBody.Parts.FixedTranslation riser(animation = false, r = {0, 0, 2}) annotation(
    Placement(visible = true, transformation(origin = {-226, 28}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Mechanics.MultiBody.Joints.Prismatic prismatic(a(fixed = false, start = 0), animation = false, n = {0, 0, 1}, s(fixed = true, start = -1.5), v(fixed = true, start = 0))  annotation(
    Placement(visible = true, transformation(origin = {-238, 8}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Mechanics.MultiBody.Forces.Damper damper(animation = false, d = 50)  annotation(
    Placement(visible = true, transformation(origin = {-238, -14}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Mechanics.MultiBody.Joints.Spherical spherical(angles_fixed = true, animation = false)  annotation(
    Placement(visible = true, transformation(origin = {-202, 8}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Mechanics.MultiBody.Forces.SpringDamperParallel support_spring_R(animation = false,c = 1000, d = 100, s_unstretched = 3)  annotation(
    Placement(visible = true, transformation(origin = {-72, 88}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Mechanics.MultiBody.Forces.SpringDamperParallel support_spring_L(animation = false,c = 1000, d = 100, s_unstretched = 3)  annotation(
    Placement(visible = true, transformation(origin = {-78, -58}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Mechanics.MultiBody.Joints.Revolute hang_point_rev(cylinderDiameter = 0.1, cylinderLength = 0.2,n = {1, 0, 0}, phi(fixed = true, start = 1.5708), useAxisFlange = true, w(fixed = true))  annotation(
    Placement(visible = true, transformation(origin = {134, 18}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Mechanics.MultiBody.Parts.FixedRotation fixedRotation(angle = 90, animation = false, n = {1, 0, 0}, r = {0, 0, 0})  annotation(
    Placement(visible = true, transformation(origin = {76, 24}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Mechanics.MultiBody.Parts.BodyShape bodyShape(m = 92, r = {0, 0, 1.4}, r_CM = {0, 0, 1.4})  annotation(
    Placement(visible = true, transformation(origin = {176, 18}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Mechanics.MultiBody.Forces.Force force(resolveInFrame = Modelica.Mechanics.MultiBody.Types.ResolveInFrameAB.frame_a)  annotation(
    Placement(visible = true, transformation(origin = {100, -42}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Sources.Step force_y(height = -200, offset = 0, startTime = 1)  annotation(
    Placement(visible = true, transformation(origin = {34, -86}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Sources.Step force_x(height = 0, offset = 0, startTime = 0)  annotation(
    Placement(visible = true, transformation(origin = {-14, -82}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Sources.Step force_z annotation(
    Placement(visible = true, transformation(origin = {94, -96}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Mechanics.Rotational.Components.Damper hang_point_damper(d = 1000)  annotation(
    Placement(visible = true, transformation(origin = {132, 62}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Frame_Model_Pack_Passive.Frame_Model frame_model_passive(initial_drop = 1, lev_length = 0.364, lev_length_TS = 0.364, noseAngle = noseAngle, slack_length = 4.72, susp_point = susp_point)  annotation(
    Placement(visible = true, transformation(origin = {9, 27}, extent = {{-59, -59}, {59, 59}}, rotation = 0)));
equation
  connect(longi.frame_a, np_support_L.frame_a) annotation(
    Line(points = {{-184, 28}, {-184, 7}, {-182, 7}, {-182, -2}}, color = {95, 95, 95}));
  connect(np_support_L.frame_b, support_L.frame_a) annotation(
    Line(points = {{-182, -22}, {-182, -46}, {-180, -46}, {-180, -58}, {-168, -58}}));
  connect(longi.frame_a, np_support_R.frame_a) annotation(
    Line(points = {{-184, 28}, {-186, 28}, {-186, 44}}, color = {95, 95, 95}));
  connect(np_support_R.frame_b, support_R.frame_a) annotation(
    Line(points = {{-186, 64}, {-186, 88}, {-174, 88}}, color = {95, 95, 95}));
  connect(world.frame_b, riser.frame_a) annotation(
    Line(points = {{-274, 28}, {-236, 28}}, color = {95, 95, 95}));
  connect(riser.frame_b, longi.frame_a) annotation(
    Line(points = {{-216, 28}, {-184, 28}}, color = {95, 95, 95}));
  connect(world.frame_b, prismatic.frame_a) annotation(
    Line(points = {{-274, 28}, {-260, 28}, {-260, 8}, {-248, 8}}, color = {95, 95, 95}));
  connect(prismatic.frame_a, damper.frame_a) annotation(
    Line(points = {{-248, 8}, {-256, 8}, {-256, -14}, {-248, -14}}, color = {95, 95, 95}));
  connect(damper.frame_b, prismatic.frame_b) annotation(
    Line(points = {{-228, -14}, {-220, -14}, {-220, 8}, {-228, 8}}, color = {95, 95, 95}));
  connect(prismatic.frame_b, spherical.frame_a) annotation(
    Line(points = {{-228, 8}, {-212, 8}}, color = {95, 95, 95}));
  connect(support_R.frame_b, support_spring_R.frame_a) annotation(
    Line(points = {{-154, 88}, {-82, 88}}));
  connect(support_L.frame_b, support_spring_L.frame_a) annotation(
    Line(points = {{-148, -58}, {-88, -58}}));
  connect(fixedRotation.frame_b, hang_point_rev.frame_a) annotation(
    Line(points = {{86, 24}, {104, 24}, {104, 18}, {124, 18}}));
  connect(hang_point_rev.frame_b, bodyShape.frame_a) annotation(
    Line(points = {{144, 18}, {166, 18}}, color = {95, 95, 95}));
  connect(force.frame_b, bodyShape.frame_b) annotation(
    Line(points = {{110, -42}, {212, -42}, {212, 18}, {186, 18}}));
  connect(force_y.y, force.force[2]) annotation(
    Line(points = {{45, -86}, {68, -86}, {68, -12}, {94, -12}, {94, -30}}, color = {0, 0, 127}));
  connect(force_x.y, force.force[1]) annotation(
    Line(points = {{-3, -82}, {18, -82}, {18, -14}, {94, -14}, {94, -30}}, color = {0, 0, 127}));
  connect(force_z.y, force.force[3]) annotation(
    Line(points = {{105, -96}, {126, -96}, {126, -12}, {94, -12}, {94, -30}}, color = {0, 0, 127}));
  connect(hang_point_damper.flange_a, hang_point_rev.support) annotation(
    Line(points = {{122, 62}, {112, 62}, {112, 28}, {128, 28}}));
  connect(hang_point_damper.flange_b, hang_point_rev.axis) annotation(
    Line(points = {{142, 62}, {154, 62}, {154, 28}, {134, 28}}));
  connect(support_spring_L.frame_b, frame_model_passive.susp_point_L) annotation(
    Line(points = {{-68, -58}, {6, -58}, {6, -14}, {6, -14}}, color = {95, 95, 95}));
  connect(support_spring_R.frame_b, frame_model_passive.susp_point_R) annotation(
    Line(points = {{-62, 88}, {6, 88}, {6, 70}, {6, 70}}, color = {95, 95, 95}));
  connect(fixedRotation.frame_a, frame_model_passive.hang_point) annotation(
    Line(points = {{66, 24}, {60, 24}, {60, 44}, {14, 44}, {14, 28}, {14, 28}}));
  connect(force.frame_a, frame_model_passive.control_point) annotation(
    Line(points = {{90, -42}, {30, -42}, {30, 16}, {8, 16}, {8, 16}}));
  connect(spherical.frame_b, frame_model_passive.nose_point) annotation(
    Line(points = {{-192, 8}, {-56, 8}, {-56, 28}, {-14, 28}, {-14, 26}}));
  annotation(
    Icon(coordinateSystem(extent = {{-500, -100}, {500, 100}})),
    Diagram(coordinateSystem(extent = {{-500, -100}, {500, 100}})),
    uses(Modelica(version = "3.2.3")),
    experiment(StartTime = 0, StopTime = 6, Tolerance = 1e-06, Interval = 0.012));
end hang_glider_passive;